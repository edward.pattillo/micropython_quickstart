# boot.py - - runs on boot-up
import gc
import network
import esp
from secret import Wifi_deets

# disable debug output meant for developers
esp.osdebug(None)

# A garbage collector is a form of automatic memory management. 
# This is a way to reclaim memory occupied by objects that are no longer in used by the program. 
# This is useful to save space in the flash memory.
gc.collect()

# connect to WIFI function
def wifi_connect(ssid, pwd, dname):

    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    wlan.config(dhcp_hostname=dname)

    if not wlan.isconnected():

        print('connecting to network...')
        wlan.connect(ssid, pwd)

        while not wlan.isconnected():
            pass

    deets = wlan.ifconfig() # get the interface's IP/netmask/gw/DNS addresses

    print("Network Config:")
    print("IP:", deets[0])
    print("Netmask:", deets[1])
    print("Gateway:", deets[2])
    print("DNS:", deets[3])


# --------- WIFI deets object -----------
wifi_creds = Wifi_deets()

# call the above function to connect to WIFI
wifi_connect(wifi_creds.essid, wifi_creds.password, wifi_creds.device_name)

