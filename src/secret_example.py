
# make a copy of this file named secret.py in the same folder
# change your SSID and network PWD below
# and be absolutely sure you add the next line to your .gitignore (uncommented of course)
# src/secret.py

class Wifi_deets:

    def __init__(self):

        self.essid = "<YourSSID>"
        self.password = "<YourPWD>"
        self.device_name = "<YourDeviceName>"
